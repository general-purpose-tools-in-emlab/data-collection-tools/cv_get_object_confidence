# cv_get_object_confidence

The `cv_get_object_confidence` is to get object confidence by [yolov5-ros](https://gitlab.com/general-purpose-tools-in-emlab/object-detector/yolo/yolov5-ros).  
In addition, this program is ROS package.  

*   Maintainer: Shoichi Hasegawa ([hasegawa.shoichi@em.ci.ritsumei.ac.jp](mailto:hasegawa.shoichi@em.ci.ritsumei.ac.jp)).
*   Author: Shoichi Hasegawa ([hasegawa.shoichi@em.ci.ritsumei.ac.jp](mailto:hasegawa.shoichi@em.ci.ritsumei.ac.jp)).


**Content:**
* [Setup](#setup)
* [Execution of program](#execution-of-program)
* [Attention](#attention)
* [Files](#files)
* [References](#References)


## Setup
~~~
cd /cv_get_object_confidence/cv_get_object_confidence/bash
bash rest_data_folder.bash
~~~

~~~
catkin_make (or catkin build)
~~~

Please input target labels in `target_objects.yaml` of `config`.



## Execution of program
~~~
cd /cv_get_object_confidence/cv_get_object_confidence/src/cv_get_object_confidence
python cv_get_object_confidence.py
~~~

## Attention
Contents of `data` folder is not managed because of existing `.gitignore`.  
Please be careful.  

## Files
 - `README.md`: Read me file (This file)

 - `__init__.py`: Set initial parameters

 - `cv_get_object_confidence.py`: main program


## Reference

