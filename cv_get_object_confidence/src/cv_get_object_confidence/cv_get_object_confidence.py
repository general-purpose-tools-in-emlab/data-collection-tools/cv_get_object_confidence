#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Standard Library
import yaml
import cv2
import time
import numpy as np
import statistics
import math
import csv

# ROS Library
import rospy
import tf
from tf import TransformListener
import tf2_ros
import tf2_geometry_msgs
from std_msgs.msg import String
from geometry_msgs.msg import Pose, PoseStamped
from sensor_msgs.msg import PointCloud2
from visualization_msgs.msg import Marker, MarkerArray
from ros_numpy.point_cloud2 import pointcloud2_to_xyz_array

# Third Package
from yolov5_ros_msgs.msg import BoundingBoxes, BoundingBox, OtherConfidenceScore, OtherConfidenceScores


class CvGetObjectConfidence():
    def __init__(self, object_conf_topic="/yolov5_ros/output/other_confidence_score"):

        with open('../../config/target_objects.yaml', 'r') as yml:
            # self.target_objects = yaml.load(yml, Loader=yaml.SafeLoader)['names']
            # self.target_objects_number = yaml.load(yml, Loader=yaml.SafeLoader)['number']
            # self.target_objects_number = yaml.safe_load(yml)['number']
            object_yaml = yaml.load(yml, Loader=yaml.SafeLoader)
            self.target_objects = object_yaml['names']
            self.target_objects_number = object_yaml['number']

        # self.neighborhood_value = 24
        self._flag = 0
        self.id_count = 0
        self._conf_list = []
        # self.bounding_topic_name = yolo_bb_topic
        # self.point_cloud_name = point_cloud_topic
        self.object_conf = object_conf_topic
        # self.marker_array_data = MarkerArray()
        # self.pub_marker_array = rospy.Publisher(marker_array_topic, MarkerArray, queue_size=1)
        # self.tflistener = TransformListener()

        self.tf_buffer = tf2_ros.Buffer()
        self.tf_listener = tf2_ros.TransformListener(self.tf_buffer)

        # self._max_label = []
        # self._conf365 = []

    def main(self):
        # while True:
        #     a = self.setup_subscriber()
        #     print(a)
        #     if a == 1:
        #         break
        # # self._conf_get()
        # self.save_data()
        self.setup_subscriber()
        while True:
            a = 0
            if ((self._flag == 1) and (a == 0)):
                self.save_data()
                a = 1
            if a == 1:
                break
        # while True:
        #     # self.setup_subscriber()
        #     a = self._cheak_number()
            # if a == 1:
            #     self.save_data()
            #     break

        # while self.flag == 0:

        #     pass

        # if self.flag == 1:
        #     self.visualization_target_object_positions()

    def setup_subscriber(self):
        self.subscriber_for_conf = rospy.Subscriber(
            self.object_conf,
            OtherConfidenceScores,
            self.conf_callback,
            queue_size=1
        )
        return

    # def bounding_callback(self, message):

    # def point_cloud_callback(self, point_cloud):

    # def get_point(self, x, y):

    # def _conf_get(self, msg):
    #     if self.is_all_target_in_image(msg.other_confidence_scores):
    #         actual_objects = []
    #         for i in msg.other_confidence_scores
    #         actual_objects.append(msg.other_confidence_scores.,max_conf_label)

    # def _cheak_number(self):
    #     actual_objects = []
    #     for i in range(len(self._conf_list)):
    #         actual_objects.append(self._conf_list[i][0])
    #     for i in range(len(actual_objects)):
    #         object_idex = self.target_objects.index(actual_objects[i])
    #         # print(actual_objects)
    #         # print(self.target_objects_number[object_idex])
    #         # print(actual_objects.count(self.target_objects[object_idex]))
    #         if self.target_objects_number[object_idex] != actual_objects.count(self.target_objects[object_idex]):
    #             print("Fail number")
    #             return 0
    #     print("Correct object number")
    #     # self._flag = 1
    #     return 1


    def conf_callback(self, msgs):
        # # self._max_label.append(msgs.max_conf_label)
        # # self._max_label.append(msgs.other_confidence_scores.max_conf_label)
        # self._max_label = msgs.other_confidence_scores.max_conf_label
        # # self._conf365.append(msgs.data)
        # # self._conf365.append(msgs.other_confidence_scores.data)
        # self._conf365 = msgs.other_confidence_scores.data
        self._conf_list = []
        if self.is_all_target_in_image(msgs.other_confidence_scores):
            for j, msg in enumerate(msgs.other_confidence_scores):
                if not msg.max_conf_label in self.target_objects:
                    continue
                self._max_label = msg.max_conf_label
                self._max_conf_object_p = msg.max_conf
                self._conf365 = msg.data
                self._conf_list.append([self._max_label, self._max_conf_object_p, self._conf365])
            actual_objects = []
            for i in range(len(self._conf_list)):
                actual_objects.append(self._conf_list[i][0])
            for i in range(len(actual_objects)):
                object_idex = self.target_objects.index(actual_objects[i])
                # print(actual_objects)
                # print(self.target_objects_number[object_idex])
                # print(actual_objects.count(self.target_objects[object_idex]))
                if self.target_objects_number[object_idex] != actual_objects.count(self.target_objects[object_idex]):
                    print("Fail number")
                    return self._flag
            print("Correct object number")
            self._flag = 1
            return self._flag


    def is_all_target_in_image(self, bboxes):
        # bboxesに含まれるラベルをlist化
        bbox_labels = []
        for bbox in bboxes:
            bbox_labels.append(bbox.max_conf_label)

        # TargetのListの物体が検出したBBの中に全て含まれるか
        for i in range(len(self.target_objects)):
            if self.target_objects[i] in bbox_labels:
                pass
            else:
                return False

        return True

    # def get_neighborhood_points(self, cx, cy, value):

    # def visualization_target_object_positions(self):

    def save_data(self):
        # object_category = ["Bottle", "Stuffed Toy", "Book", "Cup"]
        # data = []
        # labels = list(self.target_objects_positions.keys())
        # positions = list(self.target_objects_positions.values())
        # with open('../../data/object_conf_1.csv', 'w') as f:
        #     writer = csv.writer(f)
        #     for i in range(len(labels)):
        #         positions[i].insert(0, labels[i])
        #         writer.writerow(positions[i])
        with open('../../data/object_conf.csv', 'a') as f:
            writer = csv.writer(f)
            for i in range(len(self._conf_list)):
                writer.writerow(self._conf_list[i])


if __name__ == "__main__":
    rospy.init_node('cv_get_object_confidence')
    cv_get_object_confidence = CvGetObjectConfidence()
    cv_get_object_confidence.main()
    # rospy.spin()
